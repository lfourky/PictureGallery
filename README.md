1. git clone https://gitlab.com/lfourky/PictureGallery.git
2. cd PictureGallery/
3. ./gradlew build
4. create database named picture_gallery && set mysql credentials to be root:root - (CREATE SCHEMA `picture_gallery`;)
5. ./gradlew bootRun

GOTO: localhost:8080

Or, what I've used is - Eclipse.
You can import it as a Gradle project, then just run Application.class, since it has a main() method. 