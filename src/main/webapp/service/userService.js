app.service('userService', function($http) {
    return {
        loginLocally: function(user, onSuccess, onError) {
            $http.post('/login/local', user).then(onSuccess, onError);
        },
        googleLogin: function(token, onSuccess, onError) {
            $http.post('/login/google', token).then(onSuccess, onError);
        },
        registerLocally: function(user, onSuccess, onError) {
            $http.post('/register/local', user).then(onSuccess, onError);
        },
        getProfile: function(onSuccess, onError) {
            $http.get('/api/users/me').then(onSuccess, onError);
        },
        addPassword: function(onSuccess, onError) {
            $http.get('/api/users/password/add').then(onSuccess, onError);
        },
        changePassword: function(passwords, onSuccess, onError) {
            $http.post('/api/users/password/change', passwords).then(onSuccess, onError);
        },
        logout: function(onSuccess, onError) {
            $http.get('/logout').then(onSuccess, onError);
        }
    }
});
