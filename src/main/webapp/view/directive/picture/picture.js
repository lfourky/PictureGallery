app.directive("picture", function() {
    return {
        restrict: "E",
        templateUrl: "view/directive/picture/picture.html"
    };
});
