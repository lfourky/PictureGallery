app.controller('UploadController', ['$scope', '$uibModalInstance', '$log', function($scope, $uibModalInstance, $log, $flow) {

    $scope.ok = function() {
        $scope.uploader.flow.opts.target = "/api/pictures/upload";
        $scope.uploader.flow.upload();
        $uibModalInstance.close('ok');
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

}]);
