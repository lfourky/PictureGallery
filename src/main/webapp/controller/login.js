'use strict';
app.controller('LoginCtrl', function($scope, $log, $location, $http, userService, exchangeService, $localStorage) {

    $scope.user = {
        email: "",
        password: ""
    };

    $scope.checkIfLogged = function() {
      var token = localStorage.getItem("token");
      if (token != null && token !== undefined){
        if(token.length !== 0) {
          $http.defaults.headers.common.Authorization = 'Bearer ' + token;
          $location.path('/main');
        }
      }
    }

    $scope.init = function() {
        $scope.checkIfLogged();
    }();

    $scope.loginLocally = function() {
        userService.loginLocally($scope.user, function(response) {
            $log.info("Succesfully logged in locally. Resp: ", response);
            var token = response.data;
            $http.defaults.headers.common.Authorization = 'Bearer ' + token;
            localStorage.setItem("token", token);
            $location.path('/main');
        }, function(error) {
            $log.info("Error logging in locally: ", error);
        });
    }

    var clientId = '455299623148-86f5h5pt9pnv1k5r9idb53tkhqivdted.apps.googleusercontent.com';
    var scope = 'profile email';

    $scope.googleLogin = function() {
        gapi.auth.authorize({
            client_id: clientId,
            scope: scope
        }, function() {
            var token = gapi.auth.getToken().access_token;
            userService.googleLogin(token, function(response) {
                var token = response.data;
                $http.defaults.headers.common.Authorization = 'Bearer ' + token;
                localStorage.setItem("token", token);
                $location.path('/main');
            }, function(response) {
                $log.info(response);
            });
        });
    };
});
