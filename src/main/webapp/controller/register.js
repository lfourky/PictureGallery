'use strict';
app.controller('RegisterCtrl', function($scope, $log, $location, $http, $localStorage, userService) {

    $scope.user = {
        email: "",
        password: ""
    };

    $scope.matchingPassword = "";

    $scope.passwordsMatch = false;

    $scope.checkPasswordMatches = function() {
        if ($scope.user.password.length > 0 && $scope.user.password === $scope.matchingPassword) {
            $scope.passwordsMatch = true;
            return "match";
        }
        $scope.passwordsMatch = false;
        return "no-match";
    }

    $scope.registerLocally = function() {
        if (!$scope.passwordsMatch) {
            return;
        }
        userService.registerLocally($scope.user, function(response) {
            $log.info("Succesfully registered locally. Resp: ", response);
            var token = response.data;
            $http.defaults.headers.common.Authorization = 'Bearer ' + token;
            localStorage.setItem("token", token);
            $location.path('/main');
        }, function(error) {
            $log.info("Error logging in locally: ", error);
        });
    };
});
