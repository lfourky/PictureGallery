'use strict';
app.controller('IndexCtrl', ['$scope', '$location', '$log', '$uibModal', '$http', 'userService', 'exchangeService', 'toaster', function($scope,$location, $log, $uibModal, $http, userService, exchangeService, toaster, $localStorage) {

    $scope.init = function() {
        $scope.exchangeService = exchangeService;
    };
    // toaster.success({
    //     title: "title",
    //     body: "text1"
    // });

    $scope.addPassword = function() {
        userService.addPassword(function(response) {
            toaster.success({
                title: "You've got mail!",
                body: "An email with your new password has been sent out to you. Check it out!"
            });
        }, function(error) {
            toaster.error({
                title: "Couldn't send you an email.",
                body: "There was an error trying to send you an email containing your new password."
            });
        });
    }

    var PasswordChangeModalCtrl = ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {

        $scope.passwords = {};
        $scope.passwords.oldPassword = "";
        $scope.passwords.newPassword = "";

        $scope.changePwd = function() {
            console.log($scope.passwords);
            userService.changePassword($scope.passwords, function(response) {
                toaster.success({
                    title: "Success!",
                    body: "Successfully changed password."
                });
                $uibModalInstance.dismiss('cancel');
            }, function(error) {
                if (error.status === 403) {
                    toaster.error({
                        title: "Wrong password."
                    });
                } else {
                    toaster.error({
                        title: "Error changing password...",
                        body: "An error occured. Please try again later..."
                    });
                }
            });
        }

        $scope.ok = function() {
            $uibModalInstance.close('ok');
        };

        $scope.back = function() {
            window.history.back();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }];


    $scope.logout = function() {
        userService.logout(function(response) {
            localStorage.setItem("token", "");
            $http.defaults.headers.common.Authorization = "";
            $location.path('/login');
        }, function(error) {
            $log.info("Error logging out. Err: ", error);
        });
    };

    $scope.resetPassword = function() {
        var modalInstance = $uibModal.open({
            templateUrl: 'view/modal/passwordchange.html',
            controller: PasswordChangeModalCtrl,
            scope: $scope
        });

        modalInstance.result.then(function(value) {
            $log.info('Modal finished its job at: ' + new Date() + ' with value: ' + value);
        }, function(value) {
            $log.info('Modal dismissed at: ' + new Date() + ' with value: ' + value);
        });
    };


}]);
