'use strict';
app.controller('MainCtrl', ['$scope', '$location', '$uibModal', '$log', '$http', 'userService', 'pictureService', 'exchangeService', function($scope, $location, $uibModal, $log, $http, userService, pictureService, exchangeService, $localStorage) {
    $scope.pictures = [];

    $scope.init = function() {
        $scope.exchangeService = exchangeService;
        var token = localStorage.getItem("token");
        if (token == null || token.length == 0) {
            $location.path('/login');
            return;
        }
        $http.defaults.headers.common.Authorization = 'Bearer ' + token;
        userService.getProfile(function(response) {
            $scope.exchangeService.loggedUser = response.data;
        }, function(error) {
        	//Maybe obsolete token, remove it from localstorage.
        	localStorage.removeItem('token');
            $location.path('/login');
        });

        pictureService.getAll(function(response) {
            $scope.pictures = response.data;
        });
    };

    $scope.uploadStart = function($flow) {
        var token = localStorage.getItem("token");
        $flow.opts.headers = {
            'Authorization': "Bearer " + token
        };
    }

    $scope.errorUploadingEvent = function(file, message, flow) {
        var serverMsg = JSON.parse(message);
        $log.info(serverMsg.message);
        //Clear submitted files
        flow.files = [];
    }

    $scope.uploadSuccessEvent = function(file, message, flow) {
        var picture = JSON.parse(message);
        $scope.pictures.push(picture);
        //Clear submitted files
        flow.files = [];
    }

    //Will contain uploaded image
    $scope.uploader = {};

}]);
