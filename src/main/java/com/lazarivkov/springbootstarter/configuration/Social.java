package com.lazarivkov.springbootstarter.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.google.connect.GoogleConnectionFactory;

import javax.inject.Inject;

@Configuration
public class Social {

	private static final String CLIENT_KEY = "google.clientKey";
	private static final String CLIENT_SECRET = "google.clientSecret";

	@Inject
	private Environment environment;

	@Bean
	public ConnectionFactoryLocator connectionFactoryLocator() {
		final ConnectionFactoryRegistry registry = new ConnectionFactoryRegistry();

		final String clientKey = environment.getProperty(CLIENT_KEY);
		final String clientSecret = environment.getProperty(CLIENT_SECRET);
		registry.addConnectionFactory(new GoogleConnectionFactory(clientKey, clientSecret));

		return registry;
	}
}
