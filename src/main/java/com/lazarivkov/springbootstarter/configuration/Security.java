package com.lazarivkov.springbootstarter.configuration;

import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.lazarivkov.springbootstarter.configuration.filter.JwtFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class Security extends WebSecurityConfigurerAdapter {

	private static final String LOGOUT_URL = "/logout";
	private static final String LOGIN_URL = "/#login";
	private static final String SESSION_COOKIE = "JSESSIONID";

	@Bean
	public FilterRegistrationBean filterJwtTOkens() {
	     final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
	        registrationBean.setFilter(new JwtFilter());
	        registrationBean.addUrlPatterns("/api/*");
	        return registrationBean;
	}

	/**
	 * Supplies a PasswordEncoder instance to the Spring ApplicationContext. The
	 * PasswordEncoder is used by the AuthenticationProvider to perform one-way
	 * hash operations on passwords for credential comparison.
	 * 
	 * @return A PasswordEncoder.
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.formLogin().disable();
		http.httpBasic().disable();
		http.logout().logoutUrl(LOGOUT_URL).deleteCookies(SESSION_COOKIE).logoutSuccessUrl(LOGIN_URL);
		http.rememberMe();
		http.csrf().disable();
	}

}