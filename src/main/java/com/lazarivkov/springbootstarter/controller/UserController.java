package com.lazarivkov.springbootstarter.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lazarivkov.springbootstarter.controller.exception.NotFoundException;
import com.lazarivkov.springbootstarter.model.User;
import com.lazarivkov.springbootstarter.model.dto.UserDTO;
import com.lazarivkov.springbootstarter.service.CryptoService;
import com.lazarivkov.springbootstarter.service.UserService;
import com.lazarivkov.springbootstarter.service.behaviour.MailService;
import com.lazarivkov.springbootstarter.util.AuthHelper;
import com.lazarivkov.springbootstarter.util.ObjectMerger;

@RestController
@RequestMapping("api/users")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private MailService mailService;
	
	@Autowired
	private CryptoService cryptoService;

	@RequestMapping(value = "/me", method = RequestMethod.GET)
	public ResponseEntity<UserDTO> getProfile(HttpServletRequest request) throws NotFoundException {
		final String email = AuthHelper.getTokenSubject(request);
		final User user = userService.findUserByEmailOrThrow(email);
		final UserDTO userDTO = new UserDTO(user);
		return new ResponseEntity<>(userDTO, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<User> updateProfile(HttpServletRequest request, @Valid @RequestBody User updated) throws NotFoundException {
		final String email = AuthHelper.getTokenSubject(request);
		User old = userService.findUserByEmailOrThrow(email);
		ObjectMerger.mergeObjects(old, updated, old);
		userService.save(old);
		return new ResponseEntity<>(old, HttpStatus.OK);
	}

	@RequestMapping(value = "/password/change", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> changePassword(HttpServletRequest request, @RequestBody Passwords passwords) throws NotFoundException {
		final String email = AuthHelper.getTokenSubject(request);
		User user = userService.findUserByEmailOrThrow(email);
		if(!cryptoService.passwordsMatch(passwords.oldPassword, user.getPassword())) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		
		if(passwords.newPassword.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		userService.changePassword(user, passwords.newPassword);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	private static class Passwords {
		public String oldPassword;
		public String newPassword;
	}

	@RequestMapping(value = "/password/add", method = RequestMethod.GET)
	public ResponseEntity<Void> addPassword(HttpServletRequest request) throws NotFoundException {
		final String email = AuthHelper.getTokenSubject(request);
		User user = userService.findUserByEmailOrThrow(email);
		mailService.addPassword(user);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteProfile(HttpServletRequest request) throws NotFoundException {
		final String email = AuthHelper.getTokenSubject(request);
		User user = userService.findUserByEmailOrThrow(email);
		userService.delete(user);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}