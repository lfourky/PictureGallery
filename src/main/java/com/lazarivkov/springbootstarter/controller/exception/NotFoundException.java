package com.lazarivkov.springbootstarter.controller.exception;

public class NotFoundException extends Exception {

	private static final long serialVersionUID = -995060049583206271L;

	public NotFoundException() {

	}

	public NotFoundException(String message) {
		super(message);
	}
}