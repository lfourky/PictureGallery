package com.lazarivkov.springbootstarter.controller.exception;

public class BadRequestException extends Exception {

	private static final long serialVersionUID = -1220745050096049255L;

	public BadRequestException() {

	}

	public BadRequestException(String message) {
		super(message);
	}
}