package com.lazarivkov.springbootstarter.controller.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.lazarivkov.springbootstarter.controller.exception.BadRequestException;
import com.lazarivkov.springbootstarter.controller.exception.NotFoundException;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ControllerExceptionHandler {

	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<String> badRequestException(HttpServletRequest req, BadRequestException exc) {
		return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<Void> notFoundException(HttpServletRequest req, NotFoundException exc) {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}