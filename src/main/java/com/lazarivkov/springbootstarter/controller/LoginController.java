package com.lazarivkov.springbootstarter.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.social.google.api.plus.Person;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lazarivkov.springbootstarter.controller.exception.NotFoundException;
import com.lazarivkov.springbootstarter.model.User;
import com.lazarivkov.springbootstarter.service.CryptoService;
import com.lazarivkov.springbootstarter.service.UserService;
import com.lazarivkov.springbootstarter.util.AuthHelper;
import com.lazarivkov.springbootstarter.util.GoogleUtil;

@RestController
@RequestMapping("/login")
public class LoginController {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private CryptoService cryptoService;

	/**
	 * Web service endpoint that authenticates a user VIA Google's OAuth protocol.
	 */
	@RequestMapping(value = "/google", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> googleSignIn(@RequestBody String accessToken) throws NotFoundException {

		final Person googleProfile = GoogleUtil.getGoogleProfile(accessToken);

		final String displayName = googleProfile.getDisplayName();
		final String email = googleProfile.getAccountEmail();

		logger.info("{} ({}) is trying to sign in VIA Google.", displayName, email);

		Optional<User> optUser = userService.findByEmail(email);

		if (!optUser.isPresent()) {
			// User not found, create him a profile
			User user = new User(email, displayName);
			userService.save(user);
			logger.info("Successfully registered user {}.", email);
		}
		
		String token = AuthHelper.createToken(email);
		logger.info("{} signed in successfully.", displayName);
		return new ResponseEntity<>(token, HttpStatus.OK);
	}

	/**
	 * Web service endpoint that allows a user to login VIA their local account.
	 */
	@RequestMapping(value = "/local", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> loginLocally(@RequestBody User user) throws NotFoundException {
		// Does the user exist?
		Optional<User> dbUser = userService.findByEmail(user.getEmail());
		if (dbUser.isPresent()) {
			// User exists, verify password matches
			boolean passwordsMatch = cryptoService.passwordsMatch(user.getPassword(), dbUser.get().getPassword());
			if (passwordsMatch) {
				String token = AuthHelper.createToken(user.getEmail());
				logger.info("{} logged in locally.", user.getEmail());
				//AuthHelper.setAuthentication(dbUser.get());
				return new ResponseEntity<>(token, HttpStatus.OK);
			} else {
				logger.warn("{} failed password verification.", user.getEmail());
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} else {
			logger.info("Failed local login for email: {}", user.getEmail());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}