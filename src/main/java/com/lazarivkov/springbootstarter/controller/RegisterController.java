package com.lazarivkov.springbootstarter.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lazarivkov.springbootstarter.controller.exception.NotFoundException;
import com.lazarivkov.springbootstarter.model.User;
import com.lazarivkov.springbootstarter.service.CryptoService;
import com.lazarivkov.springbootstarter.service.UserService;
import com.lazarivkov.springbootstarter.util.AuthHelper;

@RestController
@RequestMapping("/register")
public class RegisterController {

	private static final Logger logger = LoggerFactory.getLogger(RegisterController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private CryptoService cryptoService;
	
	/**
	 * Web service endpoint that allows a user to register their local account.
	 */
	@RequestMapping(value = "/local", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> registerLocally(@Valid @RequestBody User user) throws NotFoundException {
		// Does the user exist?
		Optional<User> dbUser = userService.findByEmail(user.getEmail());
		if (dbUser.isPresent()) {
			//User exists, so don't allow registration
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		} else {
			String encryptedPassword = cryptoService.encryptPassword(user.getPassword());
			user.setPassword(encryptedPassword);
			user.setDisplayName(user.getEmail().split("@")[0]);
			userService.save(user);
			//AuthHelper.setAuthentication(user);
			logger.info("{} registered successfully.", user.getEmail());
			String token = AuthHelper.createToken(user.getEmail());
			return new ResponseEntity<String>(token, HttpStatus.OK);
		}
	}

}
