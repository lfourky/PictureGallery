package com.lazarivkov.springbootstarter.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lazarivkov.springbootstarter.controller.exception.NotFoundException;
import com.lazarivkov.springbootstarter.model.Picture;
import com.lazarivkov.springbootstarter.model.User;
import com.lazarivkov.springbootstarter.model.dto.PictureDTO;
import com.lazarivkov.springbootstarter.service.PictureService;
import com.lazarivkov.springbootstarter.service.UserService;
import com.lazarivkov.springbootstarter.util.AuthHelper;

@RestController
@RequestMapping("api/pictures")
public class PictureController {

	private static final Logger logger = LoggerFactory.getLogger(PictureController.class);

	/**
	 * Values denoting where in our project should the images be stored.
	 */
	@Value("${webapp.images}")
	private String IMAGES_FILE_PATH;

	/**
	 * The UserService business service.
	 */
	@Autowired
	private UserService userService;

	/**
	 * The PictureService business service.
	 */
	@Autowired
	private PictureService pictureService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<PictureDTO>> getMyPictures(HttpServletRequest request) throws NotFoundException {
		final String email = AuthHelper.getTokenSubject(request);
		final User user = userService.findUserByEmailOrThrow(email);
		final List<Picture> pictures = pictureService.findByUser(user);
		final List<PictureDTO> picturesDTO = PictureDTO.getDTOList(pictures);
		return new ResponseEntity<>(picturesDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ResponseEntity<PictureDTO> upload(HttpServletRequest request, @RequestParam("file") MultipartFile file) throws NotFoundException {
		final String email = AuthHelper.getTokenSubject(request);
		final User user = userService.findUserByEmailOrThrow(email);
		
		if (!isImage(file)) {
			throw new IllegalArgumentException("File that you sent was not recognized as an image!");
		}

		try {
			final BufferedImage image = ImageIO.read(file.getInputStream());

			final String pictureName = file.getOriginalFilename();

			final String format = getPictureFormat(file);

			final String fileName = user.getEmail() + "/" + System.currentTimeMillis() + "." + format;

			prepareUserFolder(user.getEmail());

			final File imageFile = new File(IMAGES_FILE_PATH + "/" + fileName);

			if (!ImageIO.write(image, format, imageFile)) {
				throw new IOException("Couldn't save the image. Try later!");
			}

			final Picture picture = new Picture(pictureName, fileName, new Date(), user);

			pictureService.save(picture);

			logger.info("{} uploaded a picture.", user.getEmail());

			return new ResponseEntity<>(new PictureDTO(picture), HttpStatus.OK);
		} catch (IOException | IllegalArgumentException e) {
			logger.error("There's been an error while uploading a picture! \nError message: \n{}", e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	private void prepareUserFolder(String username) {
		// Check if folder exists and create if it doesn't
		Path userFolderPath = Paths.get(IMAGES_FILE_PATH + "/" + username);

		if (!Files.exists(userFolderPath)) {
			try {
				logger.warn("User ({}) images DIR didn't exist. Trying to create it...", username);
				Files.createDirectory(userFolderPath);
				logger.info("Succesfully created directory ({})!", userFolderPath);
			} catch (Exception e) {
				logger.error("Couldn't create image folder on path: {}", userFolderPath);
			}
		}
	}

	private String getPictureFormat(MultipartFile file) {
		String format = file.getContentType().split("/")[1];

		switch (format) {
			case "jpeg":
				return "jpg";
			case "png":
				return "png";
			default:
				return "";
		}
	}

	private boolean isImage(MultipartFile file) {
		return file.getContentType().split("/")[0].equalsIgnoreCase("image");
	}
}