package com.lazarivkov.springbootstarter.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class User {

	@Id
	@GeneratedValue
	private int id;

	/**
	 * Used for displaying the user's name on the website.
	 */
	private String displayName;

	@Column(length = 60)
	private String password;

	/**
	 * Going to be used as a unique identifier for a user, instead of a username.
	 */
	@Column(unique = true)
	@NotNull
	private String email;

	/**
	 * A list of pictures for this user.
	 */
	@OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
	private List<Picture> pictures = new ArrayList<>();

	protected User() {

	}

	public User(String email, String displayName) {
		this.email = email;
		this.displayName = displayName;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Picture> getPictures() {
		return pictures;
	}

	public void setPictures(List<Picture> pictures) {
		this.pictures = pictures;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}