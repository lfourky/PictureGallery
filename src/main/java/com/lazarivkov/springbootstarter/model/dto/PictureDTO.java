package com.lazarivkov.springbootstarter.model.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.lazarivkov.springbootstarter.model.Picture;
import com.lazarivkov.springbootstarter.model.User;

public class PictureDTO {

	private String filePath;

	private String name;

	private boolean isPublic;

	private Date dateUploaded;
	
	private UserDTO owner;
	
	public PictureDTO(Picture picture) {
		this(picture.getFilePath(), picture.getName(), picture.isPublic(), picture.getDateUploaded(), picture.getOwner());
	}
	
	public PictureDTO(String filePath, String name, boolean isPublic, Date dateUploaded, User owner) {
		this.filePath = filePath;
		this.name = name;
		this.isPublic = isPublic;
		this.dateUploaded = dateUploaded;
		this.owner = UserDTO.getDTO(owner);
	}

	public static List<PictureDTO> getDTOList(List<Picture> pictures) {
		List<PictureDTO> picturesDTO = new ArrayList<>();
	
		for(Picture p : pictures)
			picturesDTO.add(new PictureDTO(p));
		
		return picturesDTO;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isPublic() {
		return isPublic;
	}

	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}

	public Date getDateUploaded() {
		return dateUploaded;
	}

	public void setDateUploaded(Date dateUploaded) {
		this.dateUploaded = dateUploaded;
	}

	public UserDTO getOwner() {
		return owner;
	}

	public void setOwner(UserDTO owner) {
		this.owner = owner;
	}
}