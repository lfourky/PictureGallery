package com.lazarivkov.springbootstarter.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.lazarivkov.springbootstarter.model.User;

public class UserDTO {

	private String email;

	private String displayName;

	// Find out if user has a local account, by simply checking if he has a password field != null
	private boolean hasLocalAccount;

	public UserDTO(User user) {
		this(user.getEmail(), user.getDisplayName(), user.getPassword());
	}

	public UserDTO(String email, String name, String password) {
		this.displayName = name;
		this.email = email;
		this.hasLocalAccount = password != null;
	}

	public static UserDTO getDTO(User user) {
		if (user == null)
			return null;

		return new UserDTO(user);
	}

	public static List<UserDTO> getDTOList(List<User> users) {
		if (users == null)
			return null;

		List<UserDTO> usersDTO = new ArrayList<>();

		for (User u : users)
			usersDTO.add(new UserDTO(u));

		return usersDTO;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public boolean isHasLocalAccount() {
		return hasLocalAccount;
	}

	public void setHasLocalAccount(boolean hasLocalAccount) {
		this.hasLocalAccount = hasLocalAccount;
	}
}