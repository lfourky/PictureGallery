package com.lazarivkov.springbootstarter.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Picture {

	@Id
	@GeneratedValue
	private int id;

	@NotNull
	private String filePath;

	private String name;

	@NotNull
	private boolean isPublic = false;

	@NotNull
	@DateTimeFormat
	private Date dateUploaded;
	
	@JsonIgnore
	@NotNull
	@ManyToOne
	private User owner;

	protected Picture() {

	}

	public Picture(String name, String filePath, Date dateUploaded, User owner) {
		this.name = name;
		this.filePath = filePath;
		this.dateUploaded = dateUploaded;
		this.owner = owner;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Date getDateUploaded() {
		return dateUploaded;
	}

	public void setDateUploaded(Date dateUploaded) {
		this.dateUploaded = dateUploaded;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isPublic() {
		return isPublic;
	}

	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}
}