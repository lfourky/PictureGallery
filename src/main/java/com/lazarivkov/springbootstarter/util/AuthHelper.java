package com.lazarivkov.springbootstarter.util;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class AuthHelper {
	
	public static final String JWT_TOKEN_SECRET = "s3cr3t";

	public static String getTokenSubject(HttpServletRequest request) {
		final Claims claims = (Claims) request.getAttribute("claims");
		final String email = (String) claims.getSubject();	
		return email;
	}
	
	public static String createToken(String email) {
		String token = Jwts.builder().setSubject(email).setIssuedAt(new Date()).signWith(SignatureAlgorithm.HS256, JWT_TOKEN_SECRET).compact();
		return token;
	}
}