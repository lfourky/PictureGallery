package com.lazarivkov.springbootstarter.util;

import org.springframework.social.google.api.Google;
import org.springframework.social.google.api.impl.GoogleTemplate;
import org.springframework.social.google.api.plus.Person;

public class GoogleUtil {

	public static Person getGoogleProfile(String accessToken) {
		final Google google = new GoogleTemplate(accessToken);
		final Person googleProfile = google.plusOperations().getGoogleProfile();
		return googleProfile;
	}

}
