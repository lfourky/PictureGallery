package com.lazarivkov.springbootstarter.util;

import org.springframework.beans.BeanUtils;

public class ObjectMerger {

    public static <T> T mergeObjects(T a, T b, T destination) {
		BeanUtils.copyProperties(a, destination);
	    BeanUtils.copyProperties(b, destination);
	    return destination;
	}
    
}