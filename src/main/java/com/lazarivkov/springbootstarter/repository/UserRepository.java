package com.lazarivkov.springbootstarter.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lazarivkov.springbootstarter.model.User;

/**
 * <p>The UserRepository interface is a Spring Data JPA data repository for User entities.
 * </p> 
 * The UserRepository provides all the data access behaviors exposed
 * by <code>JpaRepository</code> and additional custom behaviors may be defined
 * in this interface.
 */
public interface UserRepository extends JpaRepository<User, Integer> {

	public Optional<User> findById(int id);
	public Optional<User> findByEmail(String email);
	
}