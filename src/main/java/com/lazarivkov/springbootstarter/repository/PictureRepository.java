package com.lazarivkov.springbootstarter.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lazarivkov.springbootstarter.model.Picture;
import com.lazarivkov.springbootstarter.model.User;

/**
 * <p>The PictureRepository interface is a Spring Data JPA data repository for Picture entities.
 * </p> 
 * The PictureRepository provides all the data access behaviors exposed
 * by <code>JpaRepository</code> and additional custom behaviors may be defined
 * in this interface.
 */
public interface PictureRepository extends JpaRepository<Picture, Integer> {
	
	public Optional<Picture> findByName(String name);
	
	public List<Picture> findByOwner(User owner);
	public List<Picture> findByDateUploaded(LocalDateTime dateUploaded);
	public List<Picture> findByIsPublicTrueAndOwner(User owner);
	
}