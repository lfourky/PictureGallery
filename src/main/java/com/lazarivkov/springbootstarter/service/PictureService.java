package com.lazarivkov.springbootstarter.service;

import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lazarivkov.springbootstarter.model.Picture;
import com.lazarivkov.springbootstarter.model.User;
import com.lazarivkov.springbootstarter.repository.PictureRepository;
import com.lazarivkov.springbootstarter.service.behaviour.IPictureService;

@Service
public class PictureService implements IPictureService {
	
	private static final Logger logger = LoggerFactory.getLogger(PictureService.class);

	@Autowired
	private PictureRepository pictureRepository;
	
	public List<Picture> findByUser(User user) {
		logger.info("> findByOwner({})", user.getEmail());
		final List<Picture> pictures = pictureRepository.findByOwner(user);
		logger.info("< findByOwner({})", user.getEmail());
		return pictures;
	}
	
	public List<Picture> findByOwnerAndPublic(User user) {
		logger.info("> findByOwnerAndPublic({})", user.getEmail());
		final List<Picture> pictures = pictureRepository.findByIsPublicTrueAndOwner(user);
		logger.info("< findByOwnerAndPublic({})", user.getEmail());
		return pictures;
	}
	
	public List<Picture> findByDateUploaded(LocalDateTime dateUploaded) {
		logger.info("> findByDateUploaded({})", dateUploaded);
		final List<Picture> pictures = pictureRepository.findByDateUploaded(dateUploaded);
		logger.info("< findByDateUploaded({})", dateUploaded);
		return pictures;
	}
	
	public void save(Picture picture) {
		//TODO:
		pictureRepository.save(picture);
	}
	
	public void delete(Picture picture) {
		//TODO:
		pictureRepository.delete(picture.getId());
	}
}