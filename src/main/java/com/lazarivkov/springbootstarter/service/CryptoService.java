package com.lazarivkov.springbootstarter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.lazarivkov.springbootstarter.service.behaviour.ICryptoService;

@Service
public class CryptoService implements ICryptoService {

	@Autowired
	private transient PasswordEncoder passwordEncoder;

	public String encryptPassword(String password) {
		return passwordEncoder.encode(password);
	}

	public boolean passwordsMatch(String clearTextPassword, String encodedPassword) {
		return passwordEncoder.matches(clearTextPassword, encodedPassword);
	}
}
