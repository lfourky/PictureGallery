package com.lazarivkov.springbootstarter.service.behaviour;

import java.time.LocalDateTime;
import java.util.List;

import com.lazarivkov.springbootstarter.model.Picture;
import com.lazarivkov.springbootstarter.model.User;

public interface IPictureService {
	
	/**
     * Find all pictures for given User.
     */
	public List<Picture> findByUser(User user);
	
	public List<Picture> findByDateUploaded(LocalDateTime dateUploaded);
	
	public void save(Picture picture);
	
	public void delete(Picture picture);	
}