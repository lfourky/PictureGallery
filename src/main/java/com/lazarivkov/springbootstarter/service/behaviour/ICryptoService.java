package com.lazarivkov.springbootstarter.service.behaviour;

public interface ICryptoService {

	public String encryptPassword(String password);

	public boolean passwordsMatch(String password, String anotherPassword);

}
