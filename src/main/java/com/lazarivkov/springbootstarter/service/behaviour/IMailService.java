package com.lazarivkov.springbootstarter.service.behaviour;

import com.lazarivkov.springbootstarter.model.User;

public interface IMailService {
	
	public void addPassword(User user);
	
	public void sendRegistrationEmail(User user);
	
}