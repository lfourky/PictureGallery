package com.lazarivkov.springbootstarter.service.behaviour;

import java.util.List;
import java.util.Optional;

import com.lazarivkov.springbootstarter.model.User;

public interface IUserService {

	public List<User> findAll();

	public Optional<User> findById(int id);
	
	public Optional<User> findByEmail(String email);
	
	public User save(User user);

	public void delete(User user);
	
	public void changePassword(User user, String password);
}