package com.lazarivkov.springbootstarter.service.behaviour;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.lazarivkov.springbootstarter.model.User;
import com.lazarivkov.springbootstarter.service.UserService;

/**
 * Email sending using gmail account
 */
@Component
public class MailService implements IMailService {

	private static final Logger logger = LoggerFactory.getLogger(MailService.class);

	private static final String MY_EMAIL = "picturegallery@gmail.com";

	private static final String SUBJECT_ADD_PASSWORD = "Password added";
	private static final String SUBJECT_REGISTRATION_EMAIL = "Registration email";

	@Autowired
	private Environment env;

	@Autowired
	private UserService userService;

	@Override
	public void addPassword(User user) {
		Properties props = setupProperties();
		Session session = getSession(props);

		try {
			Message message = setupMessage(session);

			SecureRandom random = new SecureRandom();

			String generatedPassword = new BigInteger(42, random).toString(10);

			String content = "Hi, this is your new password: <br> <b>" + generatedPassword + "</b><br>You can now login locally, using your password. <br> <br> Thanks for using PictureGallery!";
			message.setContent(content, contentType());
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(user.getEmail()));
			message.setSubject(SUBJECT_ADD_PASSWORD);
			Transport.send(message);
			logger.info("Email sent! Time: {}", new Date());

			// Made it this far - save the password for the user
			userService.changePassword(user, generatedPassword);
		} catch (MessagingException | UnsupportedEncodingException me) {
			logger.error("Failed to send email: {} ", me);
		}
	}

	@Override
	public void sendRegistrationEmail(User user) {
		Properties props = setupProperties();
		Session session = getSession(props);

		try {
			Message message = setupMessage(session);

			String content = "Hi, " + user.getDisplayName() + "!<br>Thanks for registering on PictureGallery!";
			message.setContent(content, contentType());
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(user.getEmail()));
			message.setSubject(SUBJECT_REGISTRATION_EMAIL);
			Transport.send(message);
			logger.info("Email sent! Time: {}", new Date());
		} catch (MessagingException | UnsupportedEncodingException me) {
			logger.error("Failed to send email: {} ", me);
		}
	}

	private String contentType() {
		return "text/html; charset=utf-8";
	}

	private Message setupMessage(Session session) throws AddressException, UnsupportedEncodingException, MessagingException {
		Message message = new MimeMessage(session);
		InternetAddress me = new InternetAddress(MY_EMAIL);
		me.setPersonal(MY_EMAIL);
		message.setFrom(me);
		return message;
	}

	private Session getSession(Properties props) {
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(env.getProperty("mail.account"), env.getProperty("mail.pass"));
			}
		});
		return session;
	}

	private Properties setupProperties() {
		Properties props = new Properties();
		props.put("mail.smtp.host", env.getProperty("mail.smtp.host"));
		props.put("mail.smtp.socketFactory.class", env.getProperty("mail.smtp.socketFactory.class"));
		props.put("mail.smtp.auth", env.getProperty("mail.smtp.auth"));
		props.put("mail.smtp.connectiontimeout", env.getProperty("mail.smtp.connectiontimeout"));
		props.put("mail.smtp.timeout", env.getProperty("mail.smtp.timeout"));
		props.put("mail.smtp.ssl.trust", env.getProperty("mail.smtp.ssl.trust"));
		props.put("mail.smtp.socketFactory.port", env.getProperty("mail.smtp.socketFactory.port"));
		props.put("mail.smtp.port", env.getProperty("mail.smtp.port"));

		props.put("mail.smtp.starttls.enable", env.getProperty("mail.smtp.starttls.enable"));

		System.setProperty("mail.mime.charset", "UTF-8");
		return props;
	}
}
