package com.lazarivkov.springbootstarter.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lazarivkov.springbootstarter.controller.exception.NotFoundException;
import com.lazarivkov.springbootstarter.model.User;
import com.lazarivkov.springbootstarter.repository.UserRepository;
import com.lazarivkov.springbootstarter.service.behaviour.IUserService;

@Service
public class UserService implements IUserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CryptoService cryptoService;

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public Optional<User> findById(int id) {
		return userRepository.findById(id);
	}

	@Override
	public Optional<User> findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public User save(User user) {
		return userRepository.save(user);
	}

	@Override
	public void delete(User user) {
		userRepository.delete(user);
	}

	public void changePassword(User user, String password) {
		String encryptedPassword = cryptoService.encryptPassword(password);
		user.setPassword(encryptedPassword);
		userRepository.save(user);
	}

	/* Helpers */
	public User findUserByIdOrThrow(int userId) throws NotFoundException {
		return userRepository.findById(userId).orElseThrow(() -> new NotFoundException("Couldn't find a user with such ID."));
	}

	public User findUserByEmailOrThrow(String email) throws NotFoundException {
		return userRepository.findByEmail(email).orElseThrow(() -> new NotFoundException("Couldn't find a user with such email."));
	}
}