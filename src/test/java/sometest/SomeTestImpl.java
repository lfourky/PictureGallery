package sometest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SomeTestImpl {

	private String leetString;
	
	@Before
	public void initialize() {
		leetString = "1337";
	}
	
	@Test
	public void stringShouldBeLeet() {
		Assert.assertEquals("Should be 1337", "1337", leetString);
	}
}